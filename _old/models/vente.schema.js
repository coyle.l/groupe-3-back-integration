const mongoose = require('mongoose');

// Define collection and schema for Users
var venteSchema = mongoose.Schema({
  //_id: { type: mongoose.Schema.ObjectId, auto: true },
  name: {
    type: String
  },
  name: {
    type: String
  },
  url: {
    type: String
  },
  ean: {
    type: Number
  },
  site_id: {
    type:  mongoose.Schema.ObjectId,
    ref: 'site'
  },
  menu_id: {
    type: mongoose.Schema.ObjectId,
    ref: 'menu'
  },
  prix:{
    type: Number
  },
},{
    collection: 'vente'
});
//var User = module.exports = mongoose.model('user', userSchema);
var Vente = mongoose.model('Vente', venteSchema);


Vente.get = function (callback, limit) {
    Vente.find(callback).limit(limit);
};

module.exports = Vente;
//module.exports = mongoose.model('User', userSchema);