const mongoose = require('mongoose');

// Define collection and schema for Users
var userSchema = mongoose.Schema({
  //_id: { type: mongoose.Schema.ObjectId, auto: true },
  name: {
    type: String
  },
  password: {
    type: String
  },
  societe: {
    type: String
  },
  siret: {
    type: String
  },
  mail: {
    type: String
  },
  tel: {
    type: String
  },
  adresse: {
    type: String
  },
  cp: {
    type: String
  },
  ville: {
    type: String
  },
  site: {
    type: String
  }
},{
    collection: 'user'
});
//var User = module.exports = mongoose.model('user', userSchema);
var User = mongoose.model('User', userSchema);


User.get = function (callback, limit) {
  User.find(callback).limit(limit);
};

module.exports = User;
//module.exports = mongoose.model('User', userSchema);