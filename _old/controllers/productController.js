// userController.js
// Import user model
Vente = require('../models/vente.schema');
// Handle index actions
exports.index = function (req, res) {
    Vente.get(function (err, products) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Vente retrieved successfully",
            data: ventes
        });
    });
};
// Handle create contact actions
exports.new = function (req, res) {
    var vente = Vente({
        name : req.body.name || 'default name',
        url : req.body.url || null,
        menu_id : req.body.menu_id,
        prix : req.body.prix,
        ean : req.body.ean,
        site_id : req.body.site_id,
    });
    //user.name = req.body.name ? req.body.name : user.name;
    //user.password = req.body.password;
// save the contact and check for errors
    vente.save(function (err) {
        res.json({
            message: 'New user created!',
            data: vente
        });
    });
};
// Handle view contact info
exports.view = function (req, res) {
    Vente.findById(req.params.vente, function (err, vente) {
        if (err)
            res.send(err);
        res.json({
            message: 'vente details loading..',
            data: vente
        });
    });
};
// Handle update contact info
exports.update = function (req, res) {
    Vente.findById(req.params.user_id, function (err, vente) {
        if (err)
            res.send(err);
            vente.name = req.body.name || "default vente name";
            vente.url = req.body.url;
            vente.menu_id = req.body.menu_id;
            vente.prix = req.body.prix;
            vente.site_id = req.body.site_id;
            vente.ean = req.body.ean;

// save the user and check for errors
    vente.save(function (err) {
            if (err)
                res.json(err);
            res.json({
                message: 'vente Info updated',
                data: vente
            });
        });
    });
};
// Handle delete contact
exports.delete = function (req, res) {
    Vente.remove({
        _id: req.params.vente_id
    }, function (err, contact) {
        if (err)
            res.send(err);
    res.json({
            status: "success",
            message: 'vente deleted'
        });
    });
};