// userController.js
// Import user model
User = require('../models/user.schema');
// Handle index actions
exports.index = function (req, res) {
    User.get(function (err, users) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "User retrieved successfully",
            data: users
        });
    });
};
// Handle create contact actions
exports.new = function (req, res) {
    var user = User({
        name : req.body.nom || 'default name',
        password : req.body.password,
        societe : req.body.societe,
        siret : req.body.siret,
        mail : req.body.mail,
        tel : req.body.tel,
        adresse : req.body.adresse,
        cp : req.body.cp,
        ville : req.body.ville,
        site : req.body.site,
    });
    //user.name = req.body.name ? req.body.name : user.name;
    //user.password = req.body.password;
// save the contact and check for errors
    user.save(function (err) {
        res.json({
            message: 'New user created!',
            data: user
        });
    });
};
// Handle view contact info
exports.view = function (req, res) {
    User.findById(req.params.user_id, function (err, user) {
        if (err)
            res.send(err);
        res.json({
            message: 'user details loading..',
            data: user
        });
    });
};
// Handle update contact info
exports.update = function (req, res) {
    User.findById(req.params.user_id, function (err, user) {
        if (err)
            res.send(err);
            user.name = req.body.name ? req.body.name : user.name;
            user.password = req.body.password;
            user.societe = req.body.societe;
            user.siret = req.body.siret;
            user.mail = req.body.mail;
            user.tel = req.body.tel;
            user.adresse = req.body.adresse;
            user.cp = req.body.cp;
            user.ville = req.body.ville;
            user.site = req.body.site;


// save the user and check for errors
        user.save(function (err) {
            if (err)
                res.json(err);
            res.json({
                message: 'User Info updated',
                data: user
            });
        });
    });
};
// Handle delete contact
exports.delete = function (req, res) {
    User.remove({
        _id: req.params.user_id
    }, function (err, contact) {
        if (err)
            res.send(err);
    res.json({
            status: "success",
            message: 'User deleted'
        });
    });
};