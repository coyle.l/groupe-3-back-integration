const express = require('express');
const router = express.Router();
const venteService = require('./vente.service');

// routes
router.post('/create', create);
router.get('/', getAll);
router.get('/current', getCurrent);
router.get('/client', getNbProduitVenduParClient);
router.get('/best', getBestClient);
router.get('/categorie', getNbProduitVenduParCategorie);
router.get('/secteur', getNbProduitVenduParSecteur);
router.get('/annee', getNbProduitVenduParAnnee);
router.get('/mois', getNbProduitVenduParMois);
router.get('/annee/:id', getNbProduitVenduParAnneeParClient);
router.get('/:id', getById);
router.put('/:id', update);
router.delete('/:id', _delete);

module.exports = router;

function create(req, res, next) {
    venteService.create(req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function getAll(req, res, next) {
    venteService.getAll()
        .then(ventes => res.json(ventes))
        .catch(err => next(err));
}

function getNbProduitVenduParClient(req, res, next) {
    venteService.getNbProduitVenduParClient()
        .then(vente => vente ? res.json(vente) : res.sendStatus(404))
        .catch(err => next(err));
}

function getBestClient(req, res, next) {
    venteService.getBestClient()
        .then(vente => vente ? res.json(vente) : res.sendStatus(404))
        .catch(err => next(err));
}

function getNbProduitVenduParCategorie(req, res, next) {
    venteService.getNbProduitVenduParCategorie()
        .then(vente => vente ? res.json(vente) : res.sendStatus(404))
        .catch(err => next(err));
}

function getNbProduitVenduParSecteur(req, res, next) {
    venteService.getNbProduitVenduParSecteur()
        .then(vente => vente ? res.json(vente) : res.sendStatus(404))
        .catch(err => next(err));
}

function getNbProduitVenduParAnnee(req, res, next) {
    venteService.getNbProduitVenduParAnnee()
        .then(vente => vente ? res.json(vente) : res.sendStatus(404))
        .catch(err => next(err));
}

function getNbProduitVenduParMois(req, res, next) {
    venteService.getNbProduitVenduParMois()
        .then(vente => vente ? res.json(vente) : res.sendStatus(404))
        .catch(err => next(err));
}

function getNbProduitVenduParAnneeParClient(req, res, next) {
    venteService.getNbProduitVenduParAnneeParClient(req.params.id)
        .then(vente => vente ? res.json(vente) : res.sendStatus(404))
        .catch(err => next(err));
}

function getCurrent(req, res, next) {
    venteService.getById(req.vente.sub)
        .then(vente => vente ? res.json(vente) : res.sendStatus(404))
        .catch(err => next(err));
}

function getById(req, res, next) {
    venteService.getById(req.params.id)
        .then(vente => vente ? res.json(vente) : res.sendStatus(404))
        .catch(err => next(err));
}

function update(req, res, next) {
    venteService.update(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    venteService.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err));
}