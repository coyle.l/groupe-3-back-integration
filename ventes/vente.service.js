const config = require('config.json');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const db = require('_helpers/db');
const Vente = db.Vente;

module.exports = {
	getAll,
    getById,
    getBestClient,
    getNbProduitVenduParClient,
    getNbProduitVenduParCategorie,
    getNbProduitVenduParSecteur,
    getNbProduitVenduParAnnee,
    getNbProduitVenduParMois,
    getNbProduitVenduParAnneeParClient,
    create,
    update,
    delete: _delete
};

async function getAll() {
    return await Vente.find().select('-hash');
}

async function getById(id) {
    return await Vente.findById(id).select('-hash');
}

async function getNbProduitVenduParClient() {
    return await Vente.aggregate(
    	[
    		{ $group: { _id : "$clientId", data: { $sum: "$quantite"} } }   		
		]
	);
}

async function getBestClient() {
    return await Vente.aggregate(
    	[
    		{ $group: { _id : "$clientId", data: { $sum: "$quantite"} } }   		
		]
	).sort("-quantite").limit(1);
}

async function getNbProduitVenduParCategorie() {
    return await Vente.aggregate(
    	[
    		{ $group: { _id : "$categorie", data: { $sum: "$quantite"} } }   		
		]
	);
}

async function getNbProduitVenduParSecteur() {
    return await Vente.aggregate(
    	[
    		{ $group: { _id : "$secteur", data: { $sum: "$quantite"} } }   		
		]
	);
}

async function getNbProduitVenduParMois() {
    return await Vente.aggregate(
    	[
    		{ $group: { _id : { day: { $month: "$date"}, year: { $year: "$date" } }, data: { $sum: "$quantite"} } }   		
		]
	);
}

async function getNbProduitVenduParAnnee() {
    return await Vente.aggregate(
    	[
    		{ $group: { _id : { $year: "$date" }, data: { $sum: "$quantite"} } }   		
		]
	);
}

async function getNbProduitVenduParAnneeParClient(id) {
    return await Vente.aggregate(
    	[
    		{ $match: { clientId : id } },
    		{ $group: { _id : { $year: "$date"} , data: { $sum: "$quantite"} } }   		
		]
	);
}

async function create(venteParam) {
    // validate
    if (await Vente.findOne({ numero: venteParam.numero })) {
        throw 'Vente "' + venteParam.numero + '" is already exist';
    }

    const vente = new Vente(venteParam);

    // save vente
    await vente.save();
}

async function update(id, venteParam) {
    const vente = await Vente.findById(id);

    // validate
    if (!vente) throw 'Vente not found';
    if (vente.numero !== venteParam.numero && await Vente.findOne({ numero: venteParam.numero })) {
        throw 'Vente "' + venteParam.numero + '" is already exist';
    }

    // copy venteParam properties to vente
    Object.assign(vente, venteParam);

    await vente.save();
}

async function _delete(id) {
    await Vente.findByIdAndRemove(id);
}