const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    numero: { type: String, unique: true, required: true },
    date: { type: Date, default: Date.now },
    nomProduit: { type: String, required: true },
    totalHT: { type: Number, required: true },
    tauxTVA: { type: String, required: true },
    totalTTC: { type: Number, required: true },
    quantite: { type: Number, required: true },
    clientId: { type: String, required: true }, 
    categorie: { type: String, required: true },
    secteur: { type: String, required: true }
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Vente', schema);