const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    nom: { type: String, unique: true, required: true },
    date: { type: Date, default: Date.now },
    adresse: { type: String, required: true },
    cp: { type: String, required: true },    
    ville: { type: String, required: true },
    contactPrenom: { type: String, required: true },
    contactNom: { type: String, required: true },
    mail: { type: String, required: true },
    tel: { type: String, required: true }
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Client', schema);