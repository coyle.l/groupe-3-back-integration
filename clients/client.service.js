const config = require('config.json');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const db = require('_helpers/db');
const Client = db.Client;

module.exports = {
    getAll,
    getById,
    create,
    update,
    delete: _delete
};

async function getAll() {
    return await Client.find().select('-hash');
}

async function getById(id) {
    return await Client.findById(id).select('-hash');
}

async function create(clientParam) {
    // validate
    if (await Client.findOne({ nom: clientParam.nom })) {
        throw 'Client "' + clientParam.nom + '" is already exist';
    }

    const client = new Client(clientParam);

    // save client
    await client.save();
}

async function update(id, clientParam) {
    const client = await Client.findById(id);

    // validate
    if (!client) throw 'Client not found';
    if (client.nom !== clientParam.nom && await Client.findOne({ nom: clientParam.nom })) {
        throw 'Client "' + clientParam.nom + '" is already exist';
    }

    // copy clientParam properties to client
    Object.assign(client, clientParam);

    await client.save();
}

async function _delete(id) {
    await Client.findByIdAndRemove(id);
}