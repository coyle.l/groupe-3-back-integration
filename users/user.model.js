const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    mail: { type: String, unique: true, required: true },
    hash: { type: String, required: true },
    password: { type: String, required: true },
    societe: { type: String, required: true },
    siret: { type: String, required: true },
    tel: { type: String, required: true },
    username: { type: String, required: true },
    lastname: { type: String, required: true },
    adresse: { type: String, required: false },
    cp: { type: String, required: false },
    ville: { type: String, required: false },
    site: { type: String, required: false },
    createdDate: { type: Date, default: Date.now }
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('User', schema);